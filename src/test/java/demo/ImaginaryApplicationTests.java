package demo;

import imaginary.ImaginaryApplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ImaginaryApplication.class)
public class ImaginaryApplicationTests {

	@Test
	public void contextLoads() {
	}

}
