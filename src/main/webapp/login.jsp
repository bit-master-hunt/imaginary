<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
	rel="stylesheet">
<script src="https://code.jquery.com/jquery-2.1.3.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script>
// 	$(document).ready(function() {
// 		login = function() {
// 			username = $('#username').val();
// 			password = $('#password').val();
// 			var payload = 'username=' + username + '&password=' + password;
// 			$.ajax({
// 				url : 'login',
// 				type : "post",
// 				headers : { "Content-Type" : 'application/x-www-form-urlencoded' },
// 				data : payload,
// 				success : function(result) {					
// 					user = result;				
// 				},
// 				failure : function(result) {
// 					alert("error logging in");
// 				}				
// 			});
// 		}
// 	});
</script>
</head>
<body>

	<div class="container">
	<form action="login" method="POST">
		<input type="text" name="username" id="username">
		<input type="password" name="password" id="password">
		<input class="btn btn-sm btn-primary" type="submit">
	</form>
	</div>
</body>
</html>