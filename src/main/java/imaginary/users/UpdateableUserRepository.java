package imaginary.users;


public interface UpdateableUserRepository {
	public ImaginaryUser update(ImaginaryUser user);
}
