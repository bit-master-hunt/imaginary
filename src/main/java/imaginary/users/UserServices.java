package imaginary.users;

import imaginary.images.Image;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServices implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	public ImaginaryUser findById(String uid) {
		return userRepository.findOne(uid);
	}

	public ImaginaryUser findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public ImaginaryUser createUser(String username, String password) {
		List<String> authorities = new ArrayList<String>();
		authorities.add("ROLE_USER");
		ImaginaryUser newUser = new ImaginaryUser((String) null, username, password, authorities, new ArrayList<Image>());
		return userRepository.insert(newUser);
	}

	public Image findByImageId(String uid, String iid) {
		return userRepository.findByImageId(uid, iid);
	}

	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("LOADING A USER +++++++++++++++++++++++++++++++++");
		ImaginaryUser user = userRepository.findByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("User " + username + " not found.");
		}
		User details = new UserWithId(user.getUsername(), user.getPassword(), true, true, true, true, authorities(user.getAuthorities()), user.getId());
		return details;
	}

	public List<GrantedAuthority> authorities(List<String> auths) {
		List<GrantedAuthority> result = new ArrayList<>();
		for (String auth : auths) {
			result.add(new SimpleGrantedAuthority(auth));
		}
		return result;
	}

	public List<ImaginaryUser> findAll() {
		return userRepository.findAll();
	}

	public ImaginaryUser addImage(String uid, String url, String comment) {
		ImaginaryUser user = findById(uid);
		Image newImage = new Image(UUID.randomUUID().toString(), url, comment);		
		user.getImages().add(newImage);
		userRepository.update(user);
		return findById(uid);
	}

	public ImaginaryUser deleteImage(String uid, String iid) {
		Image image = findByImageId(uid, iid);
		ImaginaryUser user = findById(uid);
		for(Image im : user.getImages()) {
			if(im.getId() != null && iid != null && im.getId().equals(iid)) {
				user.getImages().remove(im);
				break;
			}
		}
		return userRepository.update(user);		
	}

	public ImaginaryUser updateUser(ImaginaryUser user) {
		return userRepository.update(user);
	}
}
