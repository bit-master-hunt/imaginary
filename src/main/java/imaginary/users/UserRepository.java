package imaginary.users;

import imaginary.images.Image;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserRepository extends MongoRepository<ImaginaryUser, String>, UpdateableUserRepository {
	@Query(value= "{ 'id' : ?0, 'images.id' : ?1 }" )
	public Image findByImageId(String uid, String iid);
	public ImaginaryUser findByUsername(String username);
}
