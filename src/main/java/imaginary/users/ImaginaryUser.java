package imaginary.users;

import imaginary.images.Image;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document
public class ImaginaryUser {	
	@Id
	private String id;	
	private List<Image> images;
	private String username;
	private Boolean hasAvatar;
	
	@JsonIgnore
	private String password;
	private List<String> authorities;
	
	public ImaginaryUser(String id, String username, String password, List<String> authorities, List<Image> images) {
		super();
		this.id = id;
		this.images = images;
		this.username = username;
		this.password = password;
		this.authorities = authorities;
		this.hasAvatar = false;
	}
	
	

	public Boolean getHasAvatar() {
		return hasAvatar;
	}

	public void setHasAvatar(Boolean hasAvatar) {
		this.hasAvatar = hasAvatar;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<String> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}
	
	
}
