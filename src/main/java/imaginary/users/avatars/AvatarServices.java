package imaginary.users.avatars;

import imaginary.users.ImaginaryUser;
import imaginary.users.UserServices;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.filechooser.FileSystemView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class AvatarServices {
	@Autowired
	private UserServices userServices;
	
	public static final int WIDTH = 150, HEIGHT=150;
	
	public File getAvatarFile(String uid) {
		File root = FileSystemView.getFileSystemView().getRoots()[0];
		String path = root.getAbsolutePath() + File.pathSeparator + "tmp" + File.pathSeparator + "imaginary" + File.pathSeparator + uid;
		return new File(path);
	}	
	
	
	// truncate to WIDTH x HEIGHT
	// and convert to PNG
	public BufferedImage preProcessAvatarImage(BufferedImage image) {
		int x, y, dim;
		if(image.getWidth() < image.getHeight()) {
			x = 0;
			y = (image.getHeight() - image.getWidth()) / 2;
			dim = image.getWidth();		
		} else {
			y = 0;
			x = (image.getWidth() - image.getHeight())/ 2;
			dim = image.getHeight();
		}
				
		image =  image.getSubimage(x, y, dim, dim);		
		BufferedImage result = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
		Graphics g = result.getGraphics();
		g.drawImage(image, 0, 0, 200, 200, null);
		g.dispose();
		return result;
	}

	public byte[] getImageData(String uid) throws IOException {
		ImaginaryUser user = userServices.findById(uid);
		if(user.getHasAvatar()) {
			return FileCopyUtils.copyToByteArray(getAvatarFile(uid));
		} else {
			return null;
		}
	}

	public ImaginaryUser uploadAvatar(ImaginaryUser user, MultipartFile file) throws IOException {
		BufferedImage image = preProcessAvatarImage(ImageIO.read(file.getInputStream()));
		ImageIO.write(image, "PNG", getAvatarFile(user.getId()));		
		user.setHasAvatar(true);
		return userServices.updateUser(user);
	}
}
