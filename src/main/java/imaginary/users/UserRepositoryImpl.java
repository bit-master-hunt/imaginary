package imaginary.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class UserRepositoryImpl implements UpdateableUserRepository {
	@Autowired
	@Qualifier("defaultMongoTemplate")
	private MongoOperations mongo;
		
	private Update getUpdate(ImaginaryUser x, ImaginaryUser y) {
		Update update = new Update();
		update.set("firstName", y.getUsername());
		update.set("lastName", y.getPassword());
		update.set("images", y.getImages());	
		update.set("hasAvatar", y.getHasAvatar());
		return update;
	}
	
	@Override
	public ImaginaryUser update(ImaginaryUser user) {
		Query query = new Query();
		query.addCriteria(Criteria.where("username").is(user.getUsername()));
		ImaginaryUser old = mongo.findOne(query,  ImaginaryUser.class);		
		mongo.updateFirst(query, getUpdate(old, user), ImaginaryUser.class);
		return mongo.findOne(query, ImaginaryUser.class);
	}
}