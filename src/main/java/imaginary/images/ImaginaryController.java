package imaginary.images;

import imaginary.users.ImaginaryUser;
import imaginary.users.UserServices;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.filechooser.FileSystemView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/users")
public class ImaginaryController {
	@Autowired
	private UserServices userServices;

	@Secured("ROLE_USER")
	@RequestMapping("/{uid}")
	@ResponseBody
	@PreAuthorize(value = "principal.id == #uid")		
	public ImaginaryUser getUser(@PathVariable String uid) {
		return userServices.findById(uid);
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ImaginaryUser createUser(@RequestParam String username, @RequestParam String password) {
		return userServices.createUser(username, password);
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public List<ImaginaryUser> getUsers() {
		return userServices.findAll();
	}

	@Secured("ROLE_USER")
	@RequestMapping("/{uid}/images/{iid}")
	@ResponseBody
	@PreAuthorize(value = "principal.id == #uid")		
	public Image getImage(@PathVariable String uid, @PathVariable String iid) {
		return userServices.findByImageId(uid, iid);
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value = "/{uid}/images/{iid}", method = RequestMethod.DELETE)
	@ResponseBody
	@PreAuthorize("principal.id == #uid")		
	public ImaginaryUser deleteImage(@PathVariable String uid, @PathVariable String iid) {	
		return userServices.deleteImage(uid, iid);
	}	
	
	@Secured("ROLE_USER")
	@RequestMapping(value = "/{uid}/images", method = RequestMethod.POST)
	@ResponseBody
	@PostAuthorize(value = "principal.id == #uid")		
	public ImaginaryUser createImage(@PathVariable String uid,
			@RequestParam String url,
			@RequestParam String comment) {
		return userServices.addImage(uid, url, comment);
	}
	

	
	
	
	@RequestMapping("/neverdothis")
	@ResponseBody
	public List<ImaginaryUser> createMockData() {
		String[] names = { "mmouse", "dduck", "fflintstone" };
		String[] passwords = { "123", "abc", "def" };

		for (int i = 0; i < names.length; i++) {
			if (userServices.findByUsername(names[i]) == null) {
				createUser(names[i], passwords[i]);
			}
		}

		return userServices.findAll();
	}
}
